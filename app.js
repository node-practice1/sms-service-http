const http = require('http');
const fs = require('fs');
const chalk = require('chalk');
const {sendSms} = require('./services/sms.js');

const server = http.createServer((req , res) =>{
    let {url, method} = req;
    if(url == '/' && method == 'GET')
    {
        res.setHeader("Content-Type" , "text/html");
        res.write("<html>");
        res.write("<head><title>welcome</title></head>");
        res.write("<body");
        res.write("<center>")
        res.write("<p>send me message</p>");
        res.write("<form action ='/save' method = 'POST'>")
        res.write("<input types = 'text' name ='number'/>")
        res.write("<input type = 'submit'/>")
        res.write("</form>")
        res.write("<h1>Sending sms</h1>")
        // res.write("<form action = '/send'")
        // res.write("<input type ='submit' value='Send SMS'/>")
        // res.write("</form>")
        res.write("</center>")
        res.write("</body>");
        res.write("</html>");
        res.end();
    }
    else if (url == '/save' && method == 'POST')
    {
        let data = [];
        req.on("data" , (chunk) =>{
            data.push(chunk)
        });
        req.on("end", () => {
            let primitiveData = data.toString();
            let phoneNumber = primitiveData.split("=")[1];
            fs.writeFile("./data/numbers.txt" , phoneNumber,(err) =>{
                if(err)
                {
                    throw err
                }
                else{
                    // res.write('Has Been Saved');
                }
            })
        })
        res.end();
    } 
    else if(url == '/send')
    {
        let phoneNumber = 0;
        fs.readFile("./data/numbers.txt",(err, data) =>{
            if(err){
                throw err
            }
            else{
               phoneNumber = data.toString();
               sendSms(phoneNumber)
               console.log(phoneNumber);
            }
        })
        res.end();
    }
})
server.listen(4200 ,() => {console.log(chalk.green("Server is Running On Port 4200"))});